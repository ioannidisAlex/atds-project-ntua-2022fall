from pyspark.sql import SparkSession
from operator import add
from io import StringIO
import os, sys, time

spark = SparkSession.builder.master("spark://192.168.0.1:7077").appName("query3_RDDAPI").getOrCreate()

sc = spark.sparkContext
df = spark.read.parquet("/yellow/yellow_tripdata_2022-0*.parquet")
rdd = df.rdd
start = time.time()
res = rdd.filter(lambda x: x.PULocationID != x.DOLocationID).\
        map(lambda x : (1,(x[16],1))).\
        reduceByKey(lambda x,y : ((x[0]+y[0]),x[1]+y[1])).\
        map(lambda x : (1 , x[1][0]/x[1][1]))

cnt = 0
for i in res.collect():
        print(i)
        cnt = cnt + 1 
        if cnt>5:
                break
end = time.time()
print()
print()
print(f' {end-start} seconds')
