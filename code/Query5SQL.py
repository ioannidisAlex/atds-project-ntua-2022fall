from pyspark.sql import SparkSession

from pyspark.sql.functions import *
from pyspark.sql.types import *
import time

spark = SparkSession.builder.appName("query5_DFAPI").getOrCreate()

parDF1 = spark.read.parquet("/yellow/yellow_tripdata_2022-01.parquet")
parDF2 = spark.read.parquet("/yellow/yellow_tripdata_2022-02.parquet")
parDF3 = spark.read.parquet("/yellow/yellow_tripdata_2022-03.parquet")
parDF4 = spark.read.parquet("/yellow/yellow_tripdata_2022-04.parquet")
parDF5 = spark.read.parquet("/yellow/yellow_tripdata_2022-05.parquet")
parDF6 = spark.read.parquet("/yellow/yellow_tripdata_2022-06.parquet")

parDF1.registerTempTable("trips1")
parDF2.registerTempTable("trips2")
parDF3.registerTempTable("trips3")
parDF4.registerTempTable("trips4")
parDF5.registerTempTable("trips5")
parDF6.registerTempTable("trips6")

start = time.time()

sqlString1=("\nSelect a.date as Date,\n       avg(a.tip_percentage) as Avg_tip\nFrom(\n     select tpep_pickup_datetime, tpep_dropoff_datetime\n           ,date(tpep_pickup_datetime) as date,\n	    100*Tip_amount/Fare_amount as tip_percentage \n     from trips1\n     where Month(tpep_pickup_datetime) = 1 AND Fare_amount > 0.01\n) a\nGroup by a.date\nOrder by Avg_tip desc\nLimit 5\n")

res = spark.sql(sqlString1)

res.show()

sqlString2=("\nSelect a.date as Date,\n       avg(a.tip_percentage) as Avg_tip\nFrom(\n     select tpep_pickup_datetime, tpep_dropoff_datetime\n           ,date(tpep_pickup_datetime) as date,\n	    100*Tip_amount/Fare_amount as tip_percentage \n     from trips2\n     where Month(tpep_pickup_datetime) = 2 AND Fare_amount > 0.01\n) a\nGroup by a.date\nOrder by Avg_tip desc\nLimit 5\n")

res = spark.sql(sqlString2)

res.show()

sqlString3=("\nSelect a.date as Date,\n       avg(a.tip_percentage) as Avg_tip\nFrom(\n     select tpep_pickup_datetime, tpep_dropoff_datetime\n           ,date(tpep_pickup_datetime) as date,\n	    100*Tip_amount/Fare_amount as tip_percentage \n     from trips3\n     where Month(tpep_pickup_datetime) = 3 AND Fare_amount > 0.01\n) a\nGroup by a.date\nOrder by Avg_tip desc\nLimit 5\n")

res = spark.sql(sqlString3)

res.show()

sqlString4=("\nSelect a.date as Date,\n       avg(a.tip_percentage) as Avg_tip\nFrom(\n     select tpep_pickup_datetime, tpep_dropoff_datetime\n           ,date(tpep_pickup_datetime) as date,\n	    100*Tip_amount/Fare_amount as tip_percentage \n     from trips4\n     where Month(tpep_pickup_datetime) = 4 AND Fare_amount > 0.01\n) a\nGroup by a.date\nOrder by Avg_tip desc\nLimit 5\n")

res = spark.sql(sqlString4)

res.show()

sqlString5=("\nSelect a.date as Date,\n       avg(a.tip_percentage) as Avg_tip\nFrom(\n     select tpep_pickup_datetime, tpep_dropoff_datetime\n           ,date(tpep_pickup_datetime) as date,\n	    100*Tip_amount/Fare_amount as tip_percentage \n     from trips5\n     where Month(tpep_pickup_datetime) = 5 AND Fare_amount > 0.01\n) a\nGroup by a.date\nOrder by Avg_tip desc\nLimit 5\n")

res = spark.sql(sqlString5)

res.show()

sqlString6=("\nSelect a.date as Date,\n       avg(a.tip_percentage) as Avg_tip\nFrom(\n     select tpep_pickup_datetime, tpep_dropoff_datetime\n           ,date(tpep_pickup_datetime) as date,\n	    100*Tip_amount/Fare_amount as tip_percentage \n     from trips6\n     where Month(tpep_pickup_datetime) = 6 AND Fare_amount > 0.01\n) a\nGroup by a.date\nOrder by Avg_tip desc\nLimit 5\n")

res = spark.sql(sqlString6)

res.show()

end = time.time()
print(f'{end - start} seconds')
