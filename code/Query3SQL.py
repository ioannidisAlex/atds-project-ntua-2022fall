from pyspark.sql import SparkSession

from pyspark.sql.functions import *
from pyspark.sql.types import *

from datetime import datetime
import time

spark = SparkSession.builder.appName("query3_DFAPI").getOrCreate()

parDF1 = spark.read.parquet("/yellow/*.parquet")
parDF2 = spark.read.parquet("/lookup/*.parquet")
parDF1.registerTempTable("trips")
parDF2.registerTempTable("zones")

sqlsemiMerge = "\nSELECT *, _c2 AS PLocation\nFROM trips\nInner join zones\nOn trips.PULocationID = zones._c0\n"

dfsemiMerge = spark.sql(sqlsemiMerge)
dfsemiMerge.registerTempTable("semi")

sqlMerge = "\nSELECT tpep_pickup_datetime as pickDateTime, tpep_dropoff_datetime as dropDateTime, trip_distance, total_amount, semi.PLocation , zones._c2 AS DLocation\nFROM semi\nInner join zones\nOn semi.DOLocationID = zones._c0\n"
dfMerge = spark.sql(sqlMerge)
#dfMerge.show()
dfMerge.registerTempTable("diadromes")

start = time.time()

def format_date(date):
	date = str(date)
	secdate = (date)
	fDate = 0
	if date.split("-")[0]!="":
		month = secdate.strftime("%m")
		day = secdate.strftime("%d")
		fdate = 100*month + day
		print(fdate)
		#int(str(date.split("-")[1][1:2]) + str(secdate.split("-")[2][0:2]))
	return fDate

spark.udf.register("format_date", format_date)

def dekapenthimero(s):
	if s > 615:
		res = "12"
	elif s > 600:
		res = "11"
	elif s > 515:
		res = "10"
	elif s > 500:
		res = "9"
	elif s > 415:
		res = "8"
	elif s > 400:
		res = "7"
	elif s > 315:
		res = "6"
	elif s > 300:
		res = "5"
	elif s > 215:
		res = "4"
	elif s > 200:
		res = "3"
	elif s > 115:
		res = "2"	
	elif s > 100:
		res = "1"
	else:
		res = "0"
	return res

spark.udf.register("dekapenthimero", dekapenthimero)


sqlQuery = "\nSelect a.dekapenthimero as Dekapenthimero\n,	avg(a.trip_distance) as Avg_dist,\n	avg(a.total_amount) as Avg_cost\nFrom (\n	Select dekapenthimero(100*month(pickDateTime)+day(pickDateTime)) as dekapenthimero,\n	dropDateTime,\n	trip_distance, total_amount\n	From diadromes\n	Where PLocation != DLocation\n)a\ngroup by a.dekapenthimero\norder by a.dekapenthimero\n"
res = spark.sql(sqlQuery)
res.show()

end = time.time()
print(f'{end - start} seconds')
