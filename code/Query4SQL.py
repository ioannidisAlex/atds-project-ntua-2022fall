from pyspark.sql import SparkSession

from pyspark.sql.functions import *
from pyspark.sql.types import *

from datetime import datetime
import time

spark = SparkSession.builder.appName("query4_DFAPI").getOrCreate()

parDF1 = spark.read.parquet("/yellow/*.parquet")
parDF1.registerTempTable("trips")

start = time.time()

DF = parDF1.groupBy(day(tpep_pickup_datetime).alias("date"),hour(tpep_pickup_datetime).alias("hour")).avg("passenger_count")

window = Window.partitionBy(['date']).orderBy(desc('avg(passenger_count)'),desc('hour'))



print(DF)

end = time.time()
print(f'{end - start} seconds')
