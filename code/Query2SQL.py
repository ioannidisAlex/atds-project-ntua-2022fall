from pyspark.sql import SparkSession

from pyspark.sql.functions import *
from pyspark.sql.types import *
import time

spark = SparkSession.builder.appName("query2_DFAPI").getOrCreate()

parDF1 = spark.read.parquet("/yellow/yellow_tripdata_2022-01.parquet")
parDF2 = spark.read.parquet("/yellow/yellow_tripdata_2022-02.parquet")
parDF3 = spark.read.parquet("/yellow/yellow_tripdata_2022-03.parquet")
parDF4 = spark.read.parquet("/yellow/yellow_tripdata_2022-04.parquet")
parDF5 = spark.read.parquet("/yellow/yellow_tripdata_2022-05.parquet")
parDF6 = spark.read.parquet("/yellow/yellow_tripdata_2022-06.parquet")

parDF1.registerTempTable("trips1")
parDF2.registerTempTable("trips2")
parDF3.registerTempTable("trips3")
parDF4.registerTempTable("trips4")
parDF5.registerTempTable("trips5")
parDF6.registerTempTable("trips6")

start = time.time()

sqlString1=("\nSELECT\n	tpep_pickup_datetime as pickUp, tpep_dropoff_datetime as dropOff, \n	Tolls_amount as Tools\nFROM\n	trips1\nWHERE\n	Tolls_amount != 0 AND\n	month(tpep_pickup_datetime) = 1 AND\n	Tolls_amount = (\n		SELECT\n			MAX(Tolls_amount)\n		FROM\n			trips1\n	);\n")

res = spark.sql(sqlString1)

res.show()

sqlString2=("\nSELECT\n	tpep_pickup_datetime as pickUp, tpep_dropoff_datetime as dropOff, \n	Tolls_amount as Tools\nFROM\n	trips2\nWHERE\n	Tolls_amount != 0 AND\n	month(tpep_pickup_datetime) = 2 AND\n	Tolls_amount = (\n		SELECT\n			MAX(Tolls_amount)\n		FROM\n			trips2\n	);\n")

res = spark.sql(sqlString2)

res.show()

sqlString3=("\nSELECT\n	tpep_pickup_datetime as pickUp, tpep_dropoff_datetime as dropOff, \n	Tolls_amount as Tools\nFROM\n	trips3\nWHERE\n	Tolls_amount != 0 AND\n	month(tpep_pickup_datetime) = 3 AND\n	Tolls_amount = (\n		SELECT\n			MAX(Tolls_amount)\n		FROM\n			trips3\n	);\n")

res = spark.sql(sqlString3)

res.show()

sqlString4=("\nSELECT\n	tpep_pickup_datetime as pickUp, tpep_dropoff_datetime as dropOff, \n	Tolls_amount as Tools\nFROM\n	trips4\nWHERE\n	Tolls_amount != 0 AND\n	month(tpep_pickup_datetime) = 4 AND\n	Tolls_amount = (\n		SELECT\n			MAX(Tolls_amount)\n		FROM\n			trips4\n	);\n")

res = spark.sql(sqlString4)

res.show()

sqlString5=("\nSELECT\n	tpep_pickup_datetime as pickUp, tpep_dropoff_datetime as dropOff, \n	Tolls_amount as Tools\nFROM\n	trips5\nWHERE\n	Tolls_amount != 0 AND\n	month(tpep_pickup_datetime) = 5 AND\n	Tolls_amount = (\n		SELECT\n			MAX(Tolls_amount)\n		FROM\n			trips5\n	);\n")

res = spark.sql(sqlString5)

res.show()

sqlString6=("\nSELECT\n	tpep_pickup_datetime as pickUp, tpep_dropoff_datetime as dropOff, \n	Tolls_amount as Tools\nFROM\n	trips6\nWHERE\n	Tolls_amount != 0 AND\n	month(tpep_pickup_datetime) = 6 AND\n	Tolls_amount = (\n		SELECT\n			MAX(Tolls_amount)\n		FROM\n			trips6\n	);\n")

res = spark.sql(sqlString6)

res.show()

end = time.time()
print(f'{end - start} seconds')
