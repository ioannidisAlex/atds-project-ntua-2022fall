from pyspark.sql import SparkSession

from pyspark.sql.functions import *
from pyspark.sql.types import *
import time

spark = SparkSession.builder.appName("query1_DFAPI").getOrCreate()

parDF1 = spark.read.parquet("/yellow/yellow_tripdata_2022-03.parquet")
# -//-.read.format("csv").options(inferSchema = "true", header = "false").csv("/lookup/taxi+_zone_lookup.parquet")
parDF1.registerTempTable("trips")


start = time.time()

sqlStringQueryOne = "\nSELECT Tip_amount, DOLocationID, PULocationID\nFROM\n	trips\nWHERE\n        DOLocationID = 12 \n        AND\n	Tip_amount = (\n		SELECT\n			MAX(Tip_amount)\n                FROM\n			trips\n                WHERE \n                        DOLocationID = 12\n	);\n"

res = spark.sql(sqlStringQueryOne)

res.show()

end = time.time()
print(f'{end - start} seconds')
